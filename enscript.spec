Name:              enscript
Version:           1.6.6
Release:           22
Summary:           A plain ASCII to PostScript converter
License:           GPLv3+
URL:               http://www.gnu.org/software/enscript
Source0:           http://ftp.gnu.org/gnu/enscript/enscript-1.6.6.tar.gz
Source1:           enscript-ruby-1.6.4.tar.gz
Source2:           enscript-php-1.6.4.st
BuildRequires:     gcc autoconf automake gettext gettext-devel
Provides:          nenscript = 1.13++-13
Obsoletes:         nenscript < 1.13++-13

Patch0000:         enscript-1.6.1-locale.patch
Patch0001:         enscript-wrap_header.patch
Patch0002:         enscript-1.6.4-rh457720.patch
Patch0003:         enscript-rh477382.patch
Patch0004:         enscript-build.patch
Patch0005:         enscript-bufpos-crash.patch
Patch0006:         enscript-CVE-vasnprintf.patch

%description
GNU enscript is a computer program that converts text files to PostScript,
RTF, or HTML formats. If no input files are given, enscript processes standard input.
Enscript can be extended to handle different output media and it has many options
which can be used to customize print-outs.

%package          help
Summary:          Help documents for enscript

%description      help
The enscript-help package contains manual pages and other related files for enscript.

%prep
%autosetup -p1

%{__tar} -C states/hl -zxf %{SOURCE1} ruby.st
install -pm 644 %{SOURCE2} states/hl/php.st

%build
autoreconf -fiv
export CPPFLAGS='-DPROTOTYPES'
%configure --with-media=Letter
%make_build

%install
install -d %{buildroot}%{_datadir}/locale/{de,es,fi,fr,nl,sl}/LC_MESSAGES
%make_install

%find_lang enscript

( cd %{buildroot}
  ln .%{_prefix}/bin/enscript .%{_prefix}/bin/nenscript
)

%find_lang enscript enscript.lang

for all in README THANKS; do
        iconv -f ISO88591 -t UTF8 < $all > $all.new
        touch -r $all $all.new
        mv $all.new $all
done

%files -f enscript.lang
%doc AUTHORS ChangeLog COPYING
%{_bindir}/*
%{_datadir}/enscript/*
%config(noreplace) %{_sysconfdir}/enscript.cfg
%exclude %{_datadir}/info/dir

%files help
%doc docs/FAQ.html NEWS README README.ESCAPES THANKS TODO
%{_mandir}/man1/*
%{_infodir}/enscript*

%changelog
* Tue Jan 14 2025 pengjian <pengjian23@mails.ucas.ac.cn> - 1.6.6-22
- fix CVE-2018-17942

* Wed Oct 9 2024 lvzhonglin<lvzhonglin@inspur.com> - 1.6.6-21
- update package description

* Mon Sep 7 2020 baizhonggui<baizhonggui@huawei.com> - 1.6.6-20
- Modify Source0

* Thu Nov 21 2019 liujing<liujing144@huawei.com> - 1.6.6-19
- Package init
